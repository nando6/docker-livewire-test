<div>
<x-notifications.notify />
<h2 class="mt-6 text-center text-3xl leading-9 font-extrabold text-gray-900">
            Deposit
        </h2>
<form class="mt-8" wire:submit.prevent="submit">
<input type="hidden" name="remember" value="true">
    <div class="form-group">
        <label for="exampleInputOrderid">Order ID</label>
        <input type="text" required class="form-control" id="order_id" name="order_id" placeholder="Enter Order ID" wire:model.lazy="order_id">
        @error('order_id') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group">
        <label for="exampleInputAmount">Amount</label>
        <input type="text" required class="form-control" id="amount" name="amount" placeholder="Enter Amount" wire:model.lazy="amount">
        @error('amount') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <div class="form-group">
        <label for="exampleInputtimestamp">Timestamp</label>
        <input type="text" required class="form-control" id="timestamp" name="timestamp" placeholder="Enter Timestamp" wire:model.lazy="timestamp">
        @error('timestamp') <span class="text-danger">{{ $message }}</span> @enderror
    </div>

    <button type="submit" class="btn btn-primary">Done</button>
</form>
</div>

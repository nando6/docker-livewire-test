<?php

use App\Models\MenuModel;
use App\Models\API\LogModel;
use Illuminate\Support\Facades\DB;

function username()
{
	return session()->get('username');
}

function base_asset($base_url = '') {
    return (ENVIRONMENT == 'development') ? asset($base_url) : secure_asset($base_url);
}

function base_url($base_url = '') {
    return (ENVIRONMENT == 'development') ? url($base_url) : secure_url($base_url);
}

function response_helper($status = 200, $message = '', $data = [])
{
    return array(
        'response' => $data,
        'metadata' => array(
            'status' => $status,
            'message' => $message
        )
    );
}

function print_json($status = 200, $message = '', $data = [])
{
    $response = response_helper($status, $message, $data);

    // return $response;
    return print_json_lib($response);
}

function uid()
{
	$user_id = '';
	if (array_key_exists("HTTP_USER_ID",$_SERVER)){
		$user_id = ($_SERVER['HTTP_USER_ID']) ? $_SERVER['HTTP_USER_ID'] : '';
	}

    return $user_id;
}

function log_api($request = '', $status = '', $message = '', $data = [])
{
	$log_model = new LogModel();
    $response = array(
        'status' => $status,
        'message' => $message,
        'data' => $data
    );

    $response = json_encode($response);
    $request = json_encode($request);

    $log_model->log($request, $response);
}

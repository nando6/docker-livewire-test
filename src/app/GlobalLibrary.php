<?php

use Illuminate\Support\Facades\DB;

$client_service = 'coding-collective-indonesia';
$auth_key = 'ewallet';

// checking method request of client
function check_method($method = 'GET')
{
    $request_method = $_SERVER['REQUEST_METHOD'];
    if ($request_method == $method) {
        return true;
    } else {
        return false;
    }
}

// checking header
function check_header()
{
    $client_service = $_SERVER['HTTP_CLIENT_SERVICE'];
    $auth_key = $_SERVER['HTTP_AUTH_KEY'];
    $authorization = $_SERVER['HTTP_AUTHORIZATION'];
    $base64_auth = explode(" ",$authorization);

    if ($client_service == 'coding-collective-indonesia' && $auth_key == 'ewallet' && base64_decode($base64_auth[1]) == 'helmi_ananda_putra') {
        return true;
    } else {
        return false;
    }
}

// create token
function create_token($user_id = '')
{
    $expired_at = date('Y-m-d H:i:s', strtotime('+48 hours'));
    $timestamp = date('YmdHis');
    $encoded_signature = generate_signature($user_id, $timestamp);

    // users_id
    if ($user_id == '') {
        $user_id = $_SERVER['HTTP_USER_ID'];
    }

    $data = array(
        'user_id' => $user_id,
        'token' => $encoded_signature,
        'expired_at' => $expired_at
    );

    # revisi 07/01/2019
    DB::table('user_token')->where(['user_id' => $user_id])->delete();

    $simpan = DB::table('user_token')->insertGetId($data);

    if ($simpan > 0) {
        return $data;
    } else {
        return false;
    }
}

function update_token($user_id = '')
{
    $expired_at = date('Y-m-d H:i:s', strtotime('+24 hours'));

    // users_id
    if ($user_id == '') {
        $user_id = $_SERVER['HTTP_USER_ID'];
    }

    $data = array(

        'expired_at' => $expired_at
    );

    DB::table('user_token')
    ->where('user_id', $user_id)
    ->update($data);

    return true;
}

function get_token($user_id = '')
{
    $query = DB::table('user_token', 'uat')
    ->select('uat.*')
    ->where('uat.user_id', $user_id)->first();

    if ($query) {
        return $query->token;
    } else {
        return false;
    }
}

function generate_signature($user_id = '', $timestamp = '')
{
    $signature = hash_hmac('sha256', $user_id.'&'.$timestamp, $user_id.'die', true);

    return base64_encode($signature);
}

// check token
function check_token()
{
    $users_id = $_SERVER['HTTP_USER_ID'];
    $token = $_SERVER['HTTP_TOKEN'];

    $expired_at = date('Y-m-d H:i:s', strtotime('+48 hours'));

    $data = array(
        'expired_at' => $expired_at
    );

    DB::table('user_token')
    ->where('token', $token)
    ->update($data);

   	$query = DB::table('user_token', 'uat')
                        ->select('uat.*')
                        ->where('uat.token', $token)
                        ->where('uat.user_id', $users_id)->first();

    if ($query) {
        return true;
    } else {
        return false;
    }
}

// auth
function auth_lib($method = 'GET', $flag = true)
{
    // check method request dan method yg ditentukan
    $check_method = check_method($method);
    if ($check_method == true) {
        // check header apakah sudah sesuai
        $check_header = check_header();
        if ($check_header == true) {
            // jika flag true maka check token terlebih dahulu
            // jiak flag false maka hanya mengecek header
            if ($flag == true) {
                // check token apakah sudah expired
                $check_token = check_token();
                if ($check_token == true) {
                    return true;
                } else {
                    $response = array(
                        'response' => [],
                        'metadata' => array(
                            'status' => 401,
                            'message' => 'Token anda sudah kadaluarsa'
                        )
                    );

                    return print_json_lib($response);
                }
            } else {
                return true;
            }
        } else {
            $response = array(
                'response' => [],
                'metadata' => array(
                    'status' => 406,
                    'message' => 'Not acceptable response'
                )
            );

            return print_json_lib($response);
        }
    } else {
        $response = array(
            'response' => [],
            'metadata' => array(
                'status' => 405,
                'message' => 'Method not allowed'
            )
        );

        return print_json_lib($response);
    }
}

// fungsi untuk mengeluarkan output json
function print_json_lib($response = '', $statusHeader = 200)
{
    return $response;
	// echo json_encode($response, JSON_PRETTY_PRINT);
}

<?php

namespace App\Http\Livewire;

use App\Models\Deposit;
use Livewire\Component;
use Throwable;
use GuzzleHttp\Client;

class DepositView extends Component
{
    public $order_id;
    public $amount;
    public $timestamp;

    public function submit()
    {
        try {
            $uid = auth()->user()->id;
            $token = get_token($uid);
            $uri = 'http://localhost:80/api/deposit';
            $data = ['order_id' => $this->order_id,'amount' => $this->amount,'timestamp' => $this->timestamp];
            $credentials = base64_encode('helmi_ananda_putra');
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $credentials,
                'Token' => $token,
                'User-Id' => $uid,
                'Auth-Key' => 'ewallet',
                'Client-Service' => 'coding-collective-indonesia'
            ];
            $client   = new Client();
            $options = [];
            $options['headers'] = $headers;
            $options['form_params'] = $data;
            $options['http_errors'] = false; // for get exception y api response
            $options['timeout'] = 5; // milliseconds

            $client->request('POST', $uri , $options);

            $this->dispatchBrowserEvent('notify', 'Deposit Success');
            $this->reset();

            // Or redirect with return redirect()->route('something');

        } catch (Throwable $e) {
            $this->dispatchBrowserEvent('notify', $e->getMessage());
        }
    }

    public function render()
    {
        return view('livewire.deposit-view');
    }
}

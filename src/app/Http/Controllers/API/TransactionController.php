<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

// ============ LIBRARY MODEL ==============
use App\Models\API\MainModel;

class TransactionController extends Controller
{
    function __construct()
    {
        //Model Only
        $this->Main_model   = new MainModel();
    }

    public function deposit(Request $request)
    {
        $auth = auth_lib('POST',true);
        if($auth === true){
            $order_id = ($request->post('order_id')) ? $request->post('order_id'):'';
            $amount = ($request->post('amount')) ? $request->post('amount'):0;
            $timestamp = ($request->post('timestamp')) ? $request->post('timestamp'):'';
            $response=[];
            $status =404;
            $message ="Gagal";

            $data_ts = array(
                'order_id' => $order_id,
                'amount' => $amount,
                'created_at' => $timestamp,
                'user_insert' => uid()
            );
            // get balance user wallet
            $balance_user_wallet = $this->Main_model->balance_user_wallet(uid());
            $total_balance = 0;
            if($balance_user_wallet){
                $total_balance = $amount + $balance_user_wallet->amount;
                $data_balance = array(
                    'amount' => $total_balance,
                    'updated_at' => Carbon::now('Asia/Jakarta')
                );
                $condition = (uid() != '') ? ['user_id' => uid()] : [];
                // Update Amount user wallet
                $this->Main_model->process_data_api('user_ewallet',$data_balance,$condition);
            }

            // create new deposit
            $insert_new_deposit = $this->Main_model->process_data_api('transaction_ewallet',$data_ts,[]);
            if($insert_new_deposit){
                $status =200;
                $message ="Berhasil";
            }else{
                $status = 404;
                $message = "Gagal";
            }

            $res_jsn = print_json($status,$message,$response);
            log_api($request->all(),$status,$message,$response);
            return response()->json($res_jsn);
        }else{
            return $auth;
        }
    }

    public function withdrawal(Request $request)
    {
        $auth = auth_lib('POST',true);
        if($auth === true){
            $order_id = ($request->post('order_id')) ? $request->post('order_id'):'';
            $amount = ($request->post('amount')) ? $request->post('amount'):0;
            $timestamp = ($request->post('timestamp')) ? $request->post('timestamp'):'';
            $response=[];
            $status =404;
            $message ="Gagal";

            $data_ts = array(
                'order_id' => $order_id,
                'amount' => $amount,
                'created_at' => $timestamp,
                'user_insert' => uid()
            );
            // get balance user wallet
            $balance_user_wallet = $this->Main_model->balance_user_wallet(uid());
            $total_balance = 0;
            if($balance_user_wallet){
                if($balance_user_wallet->amount >= $amount){
                    $total_balance = $balance_user_wallet->amount - $amount;
                    $data_balance = array(
                        'amount' => $total_balance,
                        'updated_at' => Carbon::now('Asia/Jakarta')
                    );
                    $condition = (uid() != '') ? ['user_id' => uid()] : [];
                    // Update Amount user wallet
                    $this->Main_model->process_data_api('user_ewallet',$data_balance,$condition);
                }else{
                    $status = 404;
                    $message = "Balance Anda Kurang dari : ".$amount;
                    $res_jsn = print_json($status,$message,$response);
                    log_api($request->all(),$status,$message,$response);
                    return response()->json($res_jsn);
                }
            }

            // create new deposit
            $insert_new_deposit = $this->Main_model->process_data_api('transaction_ewallet',$data_ts,[]);
            if($insert_new_deposit){
                $status =200;
                $message ="Berhasil";
            }else{
                $status = 404;
                $message = "Gagal";
            }

            $res_jsn = print_json($status,$message,$response);
            log_api($request->all(),$status,$message,$response);
            return response()->json($res_jsn);
        }else{
            return $auth;
        }
    }
}

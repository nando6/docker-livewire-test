<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;

// ============ LIBRARY MODEL ==============
use App\Models\API\LoginModel;

class AuthenticationController extends Controller
{
    function __construct()
    {
        //Model Only
        $this->Login_model   = new LoginModel();
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login_user(Request $request)
    {
    	$auth = auth_lib('POST',false);
    	if($auth){
    		$username = ($request->post('username')) ? $request->post('username'):'';
    		$password = ($request->post('password')) ? $request->post('password'):'';
            $response=[];

			$user = $this->Login_model->login($username,$password);
			if($user){
				// create token
                $create_token = create_token($user->id);

                // token
                $token = isset($create_token['token']) ? $create_token['token'] : '';
                if($create_token){
                	$response = array(
	                	'uid' => $user->id,
	                	'profile_name' => $user->nama,
	                	'email' => $user->email,
	                	'no_hp' => $user->no_hp,
	                	'token_data' => $create_token
                	);

                	$status =200;

					$message ="Selamat datang ".$user->nama;

                }else{
                    $status = 400;
                    $message = 'Terjadi kesalahan saat authentikasi';
                }
			}else{
				$status = 404;
				$message = "User tidak ditemukan";
			}

			$res_jsn = $res_jsn = print_json($status,$message,$response);
            log_api($request->all(),$status,$message,$response);
            return response()->json($res_jsn);

    	}
    }

    function logout_user(Request $request)

	{
		$auth = auth_lib('POST',false);
		if($auth){
			$user_id = ($request->post('user_id')) ? $request->post('user_id'):'';
            $response=[];

			$user = $this->Login_model->check_user_auth($user_id);
			if($user == 1){
                $status = 200;
                $message = 'Logout Berhasil';

			}else{
				$status = 404;
				$message = "Logout Gagal";
			}

			$res_jsn = print_json($status,$message,$response);
			log_api($request->all(),$status,$message,$response);
            return response()->json($res_jsn);
		}
	}
}

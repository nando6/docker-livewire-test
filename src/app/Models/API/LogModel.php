<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;

// ============ LIBRARY MODEL ==============
use App\Models\API\MainModel;

class LogModel extends Model
{
    // API
    public function log($request = '', $response = '')
    {
        $main_model = new MainModel();
        $uid = uid();
        $route_uri = '/'.request()->path();
        $condition = [];
        $data = array(
            'user_id' => $uid,
            'api' => $route_uri,
            'request' => $request,
            'response' => $response,
            'created_at' => Carbon::now('Asia/Jakarta')
        );

        $main_model->process_data_api('log_trx_api', $data, $condition);
    }
    // End API
}

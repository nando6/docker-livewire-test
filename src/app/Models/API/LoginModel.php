<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class LoginModel extends Model
{
    use HasFactory;

    // API
    public function login($username = '', $password = '') {
        $flag = 0;
        $data = DB::table('users', 'mut')
                        ->join('user_profil', 'mut.id', '=', 'user_profil.user_id')
                        ->select('mut.*','user_profil.nama','user_profil.no_hp')
                        ->where('mut.username', $username)
                        ->first();
        if($data){
            if (Hash::check($password, $data->password)){
                $flag = 1;
            }
        }

        if ($flag == 1) {
            return $data;
        } else {
            return [];
        }
    }

    public function check_user_auth($user_id ='')
    {
        $is_valid = DB::table('user_token', 'uat')
                        ->select('uat.*')
                        ->where('uat.user_id', $user_id)->first();

        $res = 0;
        if (! empty($is_valid)) {
            $query_delete_token = DB::table('user_token')->where('user_id', $user_id)->delete();

            // Logout Success
            if($query_delete_token){
                $res = 1;
                return $res;
            }else{
                $res = 0;
                return $res;
            }
        } else {
            $res = 0;
            return $res;
        }
    }
    // End API
}

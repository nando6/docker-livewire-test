<?php

namespace App\Models\API;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class MainModel extends Model
{
    use HasFactory;

    // API
    function process_data_api($table='', $data='', $condition='')
    {
        if(!empty($condition)) {
            return DB::table($table)->where($condition)->update($data);
        } else {
            return DB::table($table)->insert($data);
        }
    }

    public function balance_user_wallet($user_id = '') {
        $data = DB::table('user_ewallet', 'ue')
                        ->select('ue.*')
                        ->where('ue.user_id', $user_id)
                        ->first();

        if (!empty($data)) {
            return $data;
        } else {
            return [];
        }
    }
    // End API
}

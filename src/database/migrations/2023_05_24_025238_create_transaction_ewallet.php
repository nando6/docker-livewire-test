<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_ewallet', function (Blueprint $table) {
            $table->id();
            $table->string('order_id')->nullable();
            $table->decimal('amount')->default(0);
            $table->timestamps();
            $table->string('user_insert')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_ewallet');
    }
};

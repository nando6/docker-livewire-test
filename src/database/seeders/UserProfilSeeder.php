<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_profil')->insert([
            'user_id' => '1',
            'nama' => 'Hardik',
            'no_hp' => '089669130113',
            'email' => 'hardik@gmail.com',
            'created_at' => Carbon::now('Asia/Jakarta')
        ]);
    }
}

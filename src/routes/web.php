<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('home');
})->name('home');
Route::get('/register', App\Http\Livewire\Registration::class)->name('register');
Route::get('/login', App\Http\Livewire\Login::class)->name('login');
Route::get('/logout', App\Http\Livewire\Logout::class)->name('logout');

Route::get('/deposit', App\Http\Livewire\DepositView::class)->name('deposit');
Route::get('/withdrawal', App\Http\Livewire\WithdrawalView::class)->name('withdrawal');
